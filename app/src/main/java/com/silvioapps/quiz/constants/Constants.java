package com.silvioapps.quiz.constants;

public class Constants {
    public static final int SPLASH_SCREEN_SLEEP = 2000;

    public static final String USER_NICKNAME = "USER_NICKNAME";
    public static final String USER_POINTS = "USER_POINTS";

    public static final int OPTION_1 = 0;
    public static final int OPTION_2 = 1;
    public static final int OPTION_3 = 2;
    public static final int OPTION_4 = 3;
    public static final int OPTION_5 = 4;

    public static final String API_BASE_URL = "https://quiz.dynamox.solutions";

    public static final int TOTAL_QUESTIONS = 10;

}
