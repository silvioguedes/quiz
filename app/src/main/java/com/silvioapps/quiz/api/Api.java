package com.silvioapps.quiz.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.silvioapps.quiz.features.main.models.Answer;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    public static Retrofit getInstance(String baseUrl, boolean get) {
        return new Retrofit.Builder()
                .client(getHttpClient(get))
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .build();
    }

    private static OkHttpClient getHttpClient(boolean get) {
        if(get) {
            return new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            okhttp3.Response response = chain.proceed(requestGET(original));

                            return response;
                        }
                    }).build();
        }
        else {
            return new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request original = chain.request();
                            okhttp3.Response response = chain.proceed(requestPOST(original));

                            return response;
                        }
                    }).build();
        }
    }

    private static Request requestGET(Request original) {
        Request.Builder newRequest = original.newBuilder()
                .addHeader("Content-Type", "application/json");

        return newRequest.build();
    }

    private static Request requestPOST(Request original) {
        Request.Builder newRequest = original.newBuilder()
                .addHeader("Content-Type", "application/json");

        return newRequest.build();
    }

    private static Gson getGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setLenient();

        return gsonBuilder.create();
    }

}
