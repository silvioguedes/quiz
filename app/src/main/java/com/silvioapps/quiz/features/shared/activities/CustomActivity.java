package com.silvioapps.quiz.features.shared.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class CustomActivity extends AppCompatActivity {
    private boolean isVisible = false;

    protected void attachFragment(int resId, Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(resId);
        if(fragmentById == null && !isFinishing()){
            fragmentTransaction.add(resId, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        isVisible = true;
    }

    @Override
    protected void onPause(){
        super.onPause();
        isVisible = false;
    }

    public boolean isVisible(){
        return isVisible;
    }
}
