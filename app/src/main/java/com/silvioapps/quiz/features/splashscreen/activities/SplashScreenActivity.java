package com.silvioapps.quiz.features.splashscreen.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.silvioapps.quiz.constants.Constants;
import com.silvioapps.quiz.features.login.activities.LoginActivity;

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            (new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {

                    showLoginActivity();
                    SplashScreenActivity.this.finish();
                }
            }, Constants.SPLASH_SCREEN_SLEEP);
        }
    }

    protected void showLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
