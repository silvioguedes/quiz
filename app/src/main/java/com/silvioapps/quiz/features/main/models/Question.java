package com.silvioapps.quiz.features.main.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Question implements Serializable {

	@SerializedName("statement")
	private String statement;

	@SerializedName("options")
	private List<String> options;

	@SerializedName("id")
	private String id;

	public void setStatement(String statement){
		this.statement = statement;
	}

	public String getStatement(){
		return statement;
	}

	public void setOptions(List<String> options){
		this.options = options;
	}

	public List<String> getOptions(){
		return options;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}
}