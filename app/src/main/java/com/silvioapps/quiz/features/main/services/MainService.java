package com.silvioapps.quiz.features.main.services;

import com.silvioapps.quiz.features.main.models.Answer;
import com.silvioapps.quiz.features.main.models.Result;
import com.silvioapps.quiz.features.main.models.Question;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface MainService {
    public static final String QUESTION_URL = "question";
    public static final String ANSWER_URL = "answer";

    @GET(QUESTION_URL)
    Observable<Question> getQuestion();

    @POST(ANSWER_URL)
    Observable<Result> postAnswer(@Query("question_id") String id, @Body Answer answer);
}
