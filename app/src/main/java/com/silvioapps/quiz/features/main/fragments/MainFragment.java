package com.silvioapps.quiz.features.main.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.silvioapps.quiz.R;
import com.silvioapps.quiz.constants.Constants;
import com.silvioapps.quiz.databinding.FragmentMainBinding;
import com.silvioapps.quiz.features.main.models.Answer;
import com.silvioapps.quiz.features.main.models.Question;
import com.silvioapps.quiz.features.main.models.Result;
import com.silvioapps.quiz.features.main.services.MainService;
import com.silvioapps.quiz.features.points.activities.PointsActivity;
import com.silvioapps.quiz.api.Api;
import com.silvioapps.quiz.features.shared.dialogs.LoadingDialogFragment;
import com.silvioapps.quiz.features.shared.fragments.CustomFragment;
import com.silvioapps.quiz.features.shared.utils.Utils;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainFragment extends CustomFragment {
    private int questionIndex = 0;
    private int points = 0;
    private String userNickname;
    private Question question;
    private int optionChecked = -1;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        final FragmentMainBinding fragmentMainBinding = DataBindingUtil.inflate(layoutInflater,
                R.layout.fragment_main, viewGroup, false);

        if(getArguments() != null){
            userNickname = getArguments().getString(Constants.USER_NICKNAME);
        }

        if(bundle == null){
            getQuestion(fragmentMainBinding);
        }
        else{
            question = (Question)bundle.getSerializable("question");
            userNickname = bundle.getString("userNickname");
            points = bundle.getInt("points");
            questionIndex = bundle.getInt("questionIndex");
            optionChecked = bundle.getInt("optionChecked");

            onSucessGettingQuestion(question, fragmentMainBinding, userNickname, points);
        }

        if(optionChecked == -1){
            clearOptions(fragmentMainBinding.optionsRadioGroup);
        }
        else{
            enableCheckAnswerButton(fragmentMainBinding.checkAnswerButton, true);
        }

        fragmentMainBinding.optionsRadioGroup.check(optionChecked);
        for(int i=0;i<fragmentMainBinding.optionsRadioGroup.getChildCount();i++){
            View view = fragmentMainBinding.optionsRadioGroup.getChildAt(i);

            if(view instanceof RadioButton){
                final RadioButton radioButton = (RadioButton)view;
                radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(radioButton.isChecked()) {
                            optionChecked = fragmentMainBinding.optionsRadioGroup.getCheckedRadioButtonId();

                            enableCheckAnswerButton(fragmentMainBinding.checkAnswerButton, true);
                        }
                    }
                });
            }
        }

        fragmentMainBinding.checkAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(question != null) {
                    postAnswer(fragmentMainBinding, question.getId(), getAnswer(fragmentMainBinding, question));
                }
            }
        });

        fragmentMainBinding.skipQuestionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuestion(fragmentMainBinding);
            }
        });

        return fragmentMainBinding.getRoot();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (outState != null) {
            outState.putSerializable("question", question);
            outState.putString("userNickname", userNickname);
            outState.putInt("points", points);
            outState.putInt("questionIndex", questionIndex);
            outState.putInt("optionChecked", optionChecked);
        }
    }

    protected void getQuestion(final FragmentMainBinding fragmentMainBinding){
        MainService service = Api.getInstance(Constants.API_BASE_URL, true).create(MainService.class);
        service.getQuestion()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Question>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        onStartGettingQuestion();
                    }

                    @Override
                    public void onNext(Question question) {
                        questionIndex++;

                        onSucessGettingQuestion(question, fragmentMainBinding, userNickname, points);
                    }

                    @Override
                    public void onError(Throwable t) {
                        onErrorGettingQuestion();
                    }

                    @Override
                    public void onComplete() {
                        onCompleteGettingQuestion();
                    }
                });
    }

    protected void onStartGettingQuestion(){
        if(getActivity() != null) {
            LoadingDialogFragment.getInstance().showLoadingFragment(this);
        }
    }

    protected void onSucessGettingQuestion(Question question_, FragmentMainBinding fragmentMainBinding,
                                           String userNickname, int points){
        if(question_ != null) {
            question = question_;
            String statement = question.getStatement();
            List<String> options = question.getOptions();

            showQuestion(fragmentMainBinding, statement, options, userNickname, points);
        }
    }

    protected void onErrorGettingQuestion(){
        if(getActivity() != null){
            Toast.makeText(getActivity(), R.string.error_get_question, Toast.LENGTH_LONG).show();
        }

        onCompleteGettingQuestion();
    }

    protected void onCompleteGettingQuestion(){
        LoadingDialogFragment.getInstance().cancel();
        LoadingDialogFragment.destroyInstance();
    }

    protected void showQuestion(FragmentMainBinding fragmentMainBinding, String statement,
                                List<String> options, String userNickname, int points){
        if(questionIndex < Constants.TOTAL_QUESTIONS) {
            if (fragmentMainBinding != null) {
                fragmentMainBinding.statementTextView.setText(statement);
                fragmentMainBinding.option1RadioButton.setText(options.get(Constants.OPTION_1));
                fragmentMainBinding.option2RadioButton.setText(options.get(Constants.OPTION_2));
                fragmentMainBinding.option3RadioButton.setText(options.get(Constants.OPTION_3));
                fragmentMainBinding.option4RadioButton.setText(options.get(Constants.OPTION_4));
                fragmentMainBinding.option5RadioButton.setText(options.get(Constants.OPTION_5));
            }
        }
        else{
            startPointsActivity(userNickname, points);
        }
    }

    protected void startPointsActivity(String userNickname, int points){
        if(getActivity() != null) {
            Intent intent = new Intent(getActivity(), PointsActivity.class);
            intent.putExtra(Constants.USER_NICKNAME, userNickname);
            intent.putExtra(Constants.USER_POINTS, points);

            getActivity().finish();

            getActivity().startActivity(intent);
        }
    }

    protected void enableCheckAnswerButton(Button button, boolean enable){
        if(button != null){
            button.setEnabled(enable);
        }
    }

    protected void postAnswer(final FragmentMainBinding fragmentMainBinding, String id, String answer_){
        Answer answer = new Answer(answer_);

        MainService service = Api.getInstance(Constants.API_BASE_URL, false).create(MainService.class);
        service.postAnswer(id, answer)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<Result>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        onStartPostingAnswer();
                    }

                    @Override
                    public void onNext(Result result) {
                        onSuccessPostingAnswer(fragmentMainBinding, result);
                    }

                    @Override
                    public void onError(Throwable t) {
                        onErrorPostingAnswer();
                    }

                    @Override
                    public void onComplete() {
                        onCompletePostingAnswer();
                    }
                });
    }

    protected void onStartPostingAnswer(){
        if(getActivity() != null) {
            LoadingDialogFragment.getInstance().showLoadingFragment(this);
        }
    }

    protected void onSuccessPostingAnswer(FragmentMainBinding fragmentMainBinding, Result result){
        if(getActivity() != null) {
            if(result.isResult()) {
                points++;

                Toast.makeText(getActivity(), R.string.right_answer, Toast.LENGTH_LONG).show();
            }
            else{
                Toast.makeText(getActivity(), R.string.wrong_answer, Toast.LENGTH_LONG).show();
            }
        }

        enableCheckAnswerButton(fragmentMainBinding.checkAnswerButton, false);

        clearOptions(fragmentMainBinding.optionsRadioGroup);

        getQuestion(fragmentMainBinding);
    }

    protected void onErrorPostingAnswer(){
        if(getActivity() != null){
            Toast.makeText(getActivity(), R.string.error_post_answer, Toast.LENGTH_LONG).show();
        }

        LoadingDialogFragment.getInstance().cancel();
        LoadingDialogFragment.destroyInstance();
    }

    protected void onCompletePostingAnswer(){}

    protected String getAnswer(FragmentMainBinding fragmentMainBinding, Question question){
        if(fragmentMainBinding != null && question != null && question.getOptions() != null) {
            int index = Utils.getIndexCheckedRadioGroup(fragmentMainBinding.optionsRadioGroup);

            if(index > -1) {
                return question.getOptions().get(index);
            }
        }

        return null;
    }

    protected void clearOptions(RadioGroup radioGroup){
        if(radioGroup != null){
            radioGroup.clearCheck();

            optionChecked = -1;
        }
    }
}
