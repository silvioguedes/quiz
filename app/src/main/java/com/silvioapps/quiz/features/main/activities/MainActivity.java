package com.silvioapps.quiz.features.main.activities;

import android.os.Bundle;
import android.widget.Toast;

import com.silvioapps.quiz.R;
import com.silvioapps.quiz.constants.Constants;
import com.silvioapps.quiz.features.main.fragments.MainFragment;
import com.silvioapps.quiz.features.shared.activities.CustomActivity;

public class MainActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String userNickname = null;
        if (getIntent() != null) {
            userNickname = getIntent().getStringExtra(Constants.USER_NICKNAME);
        }

        if(savedInstanceState == null) {
            if (userNickname != null) {
                String message = getString(R.string.welcome) + userNickname;
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            }
        }

        MainFragment mainFragment = new MainFragment();

        Bundle bundle = new Bundle();
        bundle.putString(Constants.USER_NICKNAME, userNickname);

        mainFragment.setArguments(bundle);

        attachFragment(R.id.frameLayout, mainFragment);
    }
}
