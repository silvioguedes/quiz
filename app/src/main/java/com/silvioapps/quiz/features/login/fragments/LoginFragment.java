package com.silvioapps.quiz.features.login.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.silvioapps.quiz.constants.Constants;
import com.silvioapps.quiz.databinding.FragmentLoginBinding;
import com.silvioapps.quiz.features.main.activities.MainActivity;
import com.silvioapps.quiz.features.shared.fragments.CustomFragment;

import com.silvioapps.quiz.R;

public class LoginFragment extends CustomFragment {

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        final FragmentLoginBinding fragmentLoginBinding = DataBindingUtil.inflate(layoutInflater,
                R.layout.fragment_login, viewGroup, false);

        fragmentLoginBinding.startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!fragmentLoginBinding.nicknameEditText.getText().toString().equals("")) {
                    startMainActivity(fragmentLoginBinding.nicknameEditText.getText().toString());
                }
                else{
                    if(getActivity() != null) {
                        Toast.makeText(getActivity(), R.string.nickname_empty_error, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        return fragmentLoginBinding.getRoot();
    }

    protected void startMainActivity(String nickname){
        if(getActivity() != null) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra(Constants.USER_NICKNAME, nickname);

            getActivity().finish();

            getActivity().startActivity(intent);
        }
    }
}
