package com.silvioapps.quiz.features.main.models;

import com.google.gson.annotations.SerializedName;

public class Result {

	@SerializedName("result")
	private boolean result;

	public void setResult(boolean result){
		this.result = result;
	}

	public boolean isResult(){
		return result;
	}
}