package com.silvioapps.quiz.features.shared.utils;

import android.view.View;
import android.widget.RadioGroup;

public class Utils {
    public static int getIndexCheckedRadioGroup(RadioGroup radioGroup){
        if(radioGroup != null){
            int radioButtonId = radioGroup.getCheckedRadioButtonId();
            View radioButton = radioGroup.findViewById(radioButtonId);
            return radioGroup.indexOfChild(radioButton);
        }

        return -1;
    }
}
