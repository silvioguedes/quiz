package com.silvioapps.quiz.features.points.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.quiz.R;
import com.silvioapps.quiz.constants.Constants;
import com.silvioapps.quiz.databinding.FragmentPointsBinding;
import com.silvioapps.quiz.features.main.activities.MainActivity;
import com.silvioapps.quiz.features.shared.fragments.CustomFragment;

public class PointsFragment extends CustomFragment {
    private String userNickname;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        final FragmentPointsBinding fragmentPointsBinding = DataBindingUtil.inflate(layoutInflater,
                R.layout.fragment_points, viewGroup, false);

        int points = 0;
        if(getArguments() != null){
            points = getArguments().getInt(Constants.USER_POINTS);
            userNickname = getArguments().getString(Constants.USER_NICKNAME);
        }

        String text = userNickname + getString(R.string.points_earned) + points + getString(R.string.points);
        fragmentPointsBinding.pointsTextView.setText(text);

        fragmentPointsBinding.restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMainActivity(userNickname);
            }
        });

        return fragmentPointsBinding.getRoot();
    }

    protected void startMainActivity(String userNickname){
        if(getActivity() != null) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.putExtra(Constants.USER_NICKNAME, userNickname);

            getActivity().finish();

            getActivity().startActivity(intent);
        }
    }
}
