package com.silvioapps.quiz.features.shared.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.silvioapps.quiz.features.shared.activities.CustomActivity;

public class CustomDialogFragment extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if(savedInstanceState != null){
            dismissAllowingStateLoss();
        }
    }

    @Override
    public void onDestroyView(){
        Dialog dialog = getDialog();
        // handles https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }

        super.onDestroyView();
    }

    @Override
    public void onCancel(DialogInterface dialogInterface){
        cancel();
    }

    public void cancel(){
        if (getActivity() != null && !getActivity().isFinishing()) {
            if(isResumed()) {
                try {
                    dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void show(Fragment parent){
        if (parent != null) {
            FragmentManager fragmentManager = parent.getFragmentManager();

            if(fragmentManager != null) {
                DialogFragment dialogFragment = (DialogFragment) fragmentManager.findFragmentByTag(getClass().getName());
                boolean activityVisible = true;
                if (parent.getActivity() instanceof CustomActivity) {
                    CustomActivity customActivity = (CustomActivity) parent.getActivity();
                    activityVisible = customActivity.isVisible();
                }

                if (parent.getActivity() != null && !parent.getActivity().isFinishing() && activityVisible) {
                    if (dialogFragment == null) {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.remove(this);
                        fragmentTransaction.add(this, getClass().getName());
                        fragmentTransaction.commit();
                    } else {
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.remove(dialogFragment);
                        fragmentTransaction.add(dialogFragment, getClass().getName());
                        fragmentTransaction.commit();
                    }
                }
            }
        }
    }
}
