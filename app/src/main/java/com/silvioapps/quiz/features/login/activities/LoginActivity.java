package com.silvioapps.quiz.features.login.activities;

import android.os.Bundle;

import com.silvioapps.quiz.R;
import com.silvioapps.quiz.features.login.fragments.LoginFragment;
import com.silvioapps.quiz.features.shared.activities.CustomActivity;

public class LoginActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        attachFragment(R.id.frameLayout, new LoginFragment());
    }
}
