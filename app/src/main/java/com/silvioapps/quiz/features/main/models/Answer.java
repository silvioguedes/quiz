package com.silvioapps.quiz.features.main.models;

public class Answer {
	private String answer;

	public Answer(String answer){
		this.answer = answer;
	}

	public void setAnswer(String answer){
		this.answer = answer;
	}

	public String isAnswer(){
		return answer;
	}
}