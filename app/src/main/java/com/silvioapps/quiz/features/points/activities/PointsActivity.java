package com.silvioapps.quiz.features.points.activities;

import android.os.Bundle;

import com.silvioapps.quiz.R;
import com.silvioapps.quiz.constants.Constants;
import com.silvioapps.quiz.features.points.fragments.PointsFragment;
import com.silvioapps.quiz.features.shared.activities.CustomActivity;

public class PointsActivity extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points);

        String userNickname = null;
        int points = 0;
        if(getIntent() != null){
            points = getIntent().getIntExtra(Constants.USER_POINTS, 0);
            userNickname = getIntent().getStringExtra(Constants.USER_NICKNAME);
        }

        PointsFragment pointsFragment = new PointsFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(Constants.USER_POINTS, points);
        bundle.putString(Constants.USER_NICKNAME, userNickname);

        pointsFragment.setArguments(bundle);

        attachFragment(R.id.frameLayout, pointsFragment);
    }
}
