package com.silvioapps.quiz.features.shared.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.quiz.R;

public class LoadingDialogFragment extends CustomDialogFragment {
    private static LoadingDialogFragment instance = null;

    public static LoadingDialogFragment getInstance(){
        if(instance == null){
            instance = new LoadingDialogFragment();
        }

        return instance;
    }

    public static void destroyInstance(){
        instance = null;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = null;

        if(getActivity() != null) {
            dialog = new Dialog(getActivity());
        }

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        return layoutInflater.inflate(R.layout.dialog_fragment_loading, viewGroup, false);
    }

    public void showLoadingFragment(Fragment fragment){
        if(fragment != null && fragment.getActivity() != null) {
            FragmentManager fragmentManager = fragment.getActivity().getSupportFragmentManager();
            Fragment fragmentFound = fragmentManager.findFragmentByTag(LoadingDialogFragment.class.getName());
            if (fragmentFound == null && !fragment.getActivity().isFinishing()) {
                show(fragment);
            }
        }
    }
}
